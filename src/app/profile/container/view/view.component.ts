import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SweetalertService } from '../../../common-module/shared-service/sweetalerts.service';
import { ToastService } from '../../../common-module/shared-service/toast.service';
import { LoadingService } from '../../../common-module/shared-service/loading.service';
import { serverurl,get_complete_profile,create_profile_url, upload_document_url } from '../../../app.constants';
import { AuthenticationService } from '../../../authentication/services/authentication.service';
import { AdministrationService } from '../../../administration/services/administration.service';
import { ViewChild } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-view-profile',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewProfileComponent implements OnInit {
  public ChangePasswordForm: FormGroup;
  CreateProfileForm: FormGroup;
  action_required_menu = false;
  newpasswordFieldType: boolean;
  current_passwordFieldType: boolean;
  confirm_passwordFieldType: boolean;
  fileData: File;
  picture_link: any= null;
  form_type = 'create';
  is_primary:any = null;
  active = 1;
  posts_num = 0;
  images_num = 0;
  videos_num = 0;

  @ViewChild('staticTabs', { static: false}) staticTabs: TabsetComponent
  cover_link: string = '';
  user_data: { email: any; name: any; username: any; bio: any; };


  onNavChange(changeEvent: NgbNavChangeEvent) {
    if (changeEvent.nextId === 3) {
      changeEvent.preventDefault();
    }
  }
  
  constructor(private formBuilder: FormBuilder,
     public sweetalertService: SweetalertService, public toastService: ToastService,
      public loadingService: LoadingService,
      public router: Router,
      public administrationService: AdministrationService,
      public authenticationService: AuthenticationService) {
   
    this.CreateProfileForm = this.formBuilder.group({
      pic_file: new FormControl('',),
      name: new FormControl('', Validators.compose([Validators.required])),
      username: new FormControl('', Validators.compose([Validators.required])),
      bio: new FormControl('', Validators.compose([Validators.required])),
    });

    this.get_complete_profile();

    this.action_required_menu =  this.authenticationService.requiresPasswordChange();


  }

  handleFileupload(e:any,type:any) {
    this.fileData = e.target.files[0];
    const formData  =  new FormData();
    formData.append('document', this.fileData);
    formData.append('documentType', type);

    this.administrationService.postrecord(upload_document_url, formData).subscribe((res:any) => {
      if (res) {
        if (type == 'profile_picture'){
          this.picture_link = serverurl + res['url_link'];
        } else if (type == 'cover_picture'){
          this.cover_link = serverurl + res['url_link'];
        }
        // console.log(this.picture_link);
      } else {
        this.loadingService.hideloading();
      }
    });
  }

  get_basic_user_details() {
    const details = this.authenticationService.getuserprofileInfo().then((res:any) => {
      const user_info = {
        "email": res['currentemail'],
        "name": res['name'],
        "username": res['username']
      }
      this.CreateProfileForm.patchValue(user_info)
    });
  }

  get_complete_profile(){
    const payload = {

    }
    this.administrationService.getrecords(get_complete_profile,payload).subscribe((res:any) => {
      if(res) {
        console.log(res);
        const pic_link =  res['profile_picture']['profile_picture'];
        const cover_link =  res['cover_picture']['cover_picture'];
        const analytics = res['analytics'];
        // console.log(pic_link);
        if (pic_link && pic_link !== '' && pic_link !== undefined ){
          this.picture_link = serverurl + res['profile_picture']['profile_picture'];        
        }
        if (cover_link && cover_link !== '' && cover_link !== undefined ){
          this.cover_link = serverurl + res['cover_picture']['cover_picture'];        
        }

        this.posts_num = analytics['posts']
        this.images_num = analytics['photos']
        this.videos_num = analytics['videos']

        this.form_type = 'edit';

        const user_info = {
          "email": res['user']['email'],
          "name": res['user']['name'],
          "username": res['user']['username'],
          "bio": res['user']['bio'],          
       }
       this.user_data = user_info;
      }
    })
  }

  edit_profile(){
    this.router.navigate(['/profile/create']);
  }


  // get_values(controls){
  //   const values = [];
  //   for (const name of controls){
  //     const control = this.CreateProfileForm.get(name);
  //     if(control.value !== ''){
  //       values.push(control.value);
  //     }
  //   }
  //   if(controls.length !== values.length){
  //     for (const name of controls){
  //       const control = this.CreateProfileForm.get(name);
  //       if(control.value === undefined || !control.value.trim() ){
  //         control.markAsDirty({ onlySelf: true });
  //         this.toastService.showToastNotification('error', name.toUpperCase() + ' Is Required', '');
  //       }
  //     }
  //     return false;
  //   } else {
  //     return true;
  //   }
  // }

  // changer(nav,id){
  //   if(id == 1){
  //     nav.select(id);
  //   } else if(id == 3){
  //     let controls = ['first_name','last_name','bio','gender','email','phonecode','phone','age_group','disability'];
  //     const results = this.get_values(controls);
  //     if(results){
  //       nav.select(id);
  //     }
  //   } 
  // }
 

  ngOnInit(): void {
  }

}
