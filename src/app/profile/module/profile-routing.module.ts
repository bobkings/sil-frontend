import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NgxPermissionsGuard } from 'ngx-permissions';
import { AuthenticationGuard } from '../../authentication/guards/authguard.guard';
import { CreateProfileComponent } from '../container/create/create.component';
import { ViewProfileComponent } from '../container/view/view.component';
const routes: Routes = [

 
  {
    path: 'create',
    component: CreateProfileComponent,
    data: {
      title: 'Create Profile',
      permissions: {
        // only: [''],
        redirectTo: '/500'
      }
    },
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'view',
    component: ViewProfileComponent,
    data: {
      title: 'View Profile',
      permissions: {
        // only: [''],
        redirectTo: '/500'
      }
    },
    canActivate: [AuthenticationGuard]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
