import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { GeneralModule } from '../../common-module/common-module/common-module.module';
import { CreateProfileComponent } from '../container/create/create.component';
import { ViewProfileComponent } from '../container/view/view.component';

import { AccordionModule } from 'ngx-bootstrap/accordion'
import { ProfileRoutingModule } from './profile-routing.module';

@NgModule({
  declarations: [
    CreateProfileComponent, ViewProfileComponent,
  ],

  imports: [
    GeneralModule,
    CommonModule,
    FormsModule,
    BsDatepickerModule,
    ReactiveFormsModule,
    ProfileRoutingModule,
    AccordionModule,
    
  ]
})
export class ProfileModule { }

