import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  serverurl, fetch_users_url, dummyendpoint
} from '../../app.constants';
import { AdministrationService } from '../../administration/services/administration.service';
import { LoadingService } from '../../common-module/shared-service/loading.service';
import { Router } from '@angular/router';
import { SweetalertService } from '../../common-module/shared-service/sweetalerts.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../../common-module/shared-service/toast.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  users = [];
  albums: Record<string, any> = {}; //dynamic object to store albums by user id as key


  constructor(
    public administrationService: AdministrationService,
    public loadingService:LoadingService,
    public router: Router,
    public sweetalertService: SweetalertService, 
    private formBuilder: FormBuilder,
    public toastService: ToastService) { 
      this.fetch_users();
    }

  ngOnInit() {
  }

  fetch_users() {
    // Retrieves all available users from dummy endpoint
    const payload = {}
    this.loadingService.showloading();
    this.administrationService.getrecords(fetch_users_url,payload).subscribe((res) => {
      console.log(res);
      this.users = res;
      for( let user of res ){
        this.fetch_albums(user['id']) // calls method to retrieve user albums passing user id parameter
      }
      this.loadingService.hideloading();
      console.log(this.albums)
      
    });
  }

  fetch_albums(user_id:any) {
    // Retrieves specific user albums
    const payload = {}
    const url:string = dummyendpoint + 'users/' + user_id + '/albums';
    this.administrationService.getrecords(url,payload).subscribe((res) => {  
      this.albums[user_id] =  res.length
    });
    
  }

  visitPage(user_id:any){
    // Routes to a selected user page
    this.router.navigate(['/channel/profile', user_id]);
  }


 


}
