import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonProfileComponent } from './common-profile.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgxPermissionsAllowStubDirective, NgxPermissionsModule } from 'ngx-permissions';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';

describe('CommonProfileComponent', () => {
  let component: CommonProfileComponent;
  let fixture: ComponentFixture<CommonProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonProfileComponent,NgxPermissionsAllowStubDirective ],
      providers: [
        JwtHelperService
      ],
      imports: [RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        HttpClientTestingModule,
        JwtModule.forRoot({ // for JwtHelperService
          config: {
            tokenGetter: () => {
              return '';
            }
          }
        }),
        NgxPermissionsModule.forRoot()
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
