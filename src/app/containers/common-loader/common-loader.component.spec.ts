import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonLoaderComponent } from './common-loader.component';
import { NgxSpinnerModule } from 'ngx-spinner';

describe('CommonLoaderComponent', () => {
  let component: CommonLoaderComponent;
  let fixture: ComponentFixture<CommonLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonLoaderComponent ],
      imports: [NgxSpinnerModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
