import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonSidebarComponent } from './common-sidebar.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { NgxPermissionsAllowStubDirective, NgxPermissionsModule } from 'ngx-permissions';

describe('CommonSidebarComponent', () => {
  let component: CommonSidebarComponent;
  let fixture: ComponentFixture<CommonSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonSidebarComponent,NgxPermissionsAllowStubDirective ],
      providers: [
        JwtHelperService
      ],
      imports: [RouterTestingModule, HttpClientTestingModule,
        JwtModule.forRoot({ // for JwtHelperService
          config: {
            tokenGetter: () => {
              return '';
            }
          }
        }),
        NgxPermissionsModule.forRoot()
      
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
