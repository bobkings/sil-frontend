import { TestBed } from '@angular/core/testing';

import { AdministrationService } from './administration.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AdministrationService', () => {
  let service: AdministrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdministrationService],
      imports: [RouterTestingModule,HttpClientTestingModule]
    });
    service = TestBed.inject(AdministrationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
