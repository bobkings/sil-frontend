import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DefaultLayoutComponent } from './containers';
import { Page404Component } from './views/pages/page404/page404.component';
import { Page500Component } from './views/pages/page500/page500.component';
import { LoginComponent } from './views/pages/login/login.component';
import { CommonProfileComponent } from './containers/common-profile/common-profile.component';
import { AuthenticationGuard } from './authentication/guards/authguard.guard';
import { ChangePasswordGuard } from './authentication/guards/change-password.guard';
const routes: Routes = [

  {  
    path: 'authentication',
    loadChildren: () => import('./authentication/authentication/authentication.module').then(m => m.AuthenticationModule
    )
  },
  {
    path: 'generic',
    loadChildren:
    () => import('./generic/module/generic.module').then(m => m.GenericModule),
    // canActivate: [AuthenticationGuard, ChangePasswordGuard],
  },
  {
    path: 'moments',
    loadChildren:
    () => import('./nonauth-landing/landing/landing.module').then(m => m.LandingModule),
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'profile',
        component: CommonProfileComponent,
        data: {
          title: 'Profile Details'
        }
      },
      {
        path: '',
        redirectTo: '/landing/home', pathMatch: 'full'
      },
      {
        path: 'landing',
        loadChildren:
        () => import('./landing/landing/landing.module').then(m => m.LandingModule),
      canActivate: [AuthenticationGuard, ChangePasswordGuard],
      },
      {
        path: 'administration',
        loadChildren:
        () => import('./administration/administration/administration/administration.module').then(m => m.AdministrationModule),
        canActivate: [AuthenticationGuard, ChangePasswordGuard],
      },
      {
        path: 'profile',
        loadChildren:
        () => import('./profile/module/profile.module').then(m => m.ProfileModule),
        canActivate: [AuthenticationGuard, ChangePasswordGuard],
      },
      {
        path: 'channel',
        loadChildren:
        () => import('./generic/module/generic.module').then(m => m.GenericModule),
        // canActivate: [AuthenticationGuard, ChangePasswordGuard],
      },
      
    ]
  },
  {
    path: '404',
    component: Page404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: Page500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  { path: '**', component:  Page404Component }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes
      , {
      scrollPositionRestoration: 'top',
      anchorScrolling: 'enabled',
      // initialNavigation: 'enabledBlocking'
      // relativeLinkResolution: 'legacy'
    }
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}