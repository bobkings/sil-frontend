import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  
} from '../../app.constants';
import { AdministrationService } from '../../administration/services/administration.service';
import { LoadingService } from '../../common-module/shared-service/loading.service';
import { Router } from '@angular/router';
import { SweetalertService } from '../../common-module/shared-service/sweetalerts.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../../common-module/shared-service/toast.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {


  constructor(
    public administrationService: AdministrationService,
    public loadingService:LoadingService,
    public router: Router,
    public sweetalertService: SweetalertService, 
    private formBuilder: FormBuilder,
    public toastService: ToastService) { 

      
    }

  ngOnInit() {

  }

  get_started(){
    this.router.navigate(['/authentication/login']);
  }
 


}
