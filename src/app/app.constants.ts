// export let serverurl = 'http://127.0.0.1:8000';
export let serverurl = 'http://20.102.106.83:5901';
export let dummyendpoint = 'https://jsonplaceholder.typicode.com/'

// ng build --configuration production

export let API_VERSION = '/api/v1/';
export let loginurl = serverurl + API_VERSION + 'authentication/login';
export let signupurl = serverurl + API_VERSION + 'authentication/create-account';

export let verify_email_url = serverurl + API_VERSION + 'authentication/verify-email';
export let resend_otp_url = serverurl + API_VERSION + 'authentication/resend-otp';
export let user_reset_password_url = serverurl + API_VERSION + 'authentication/reset-user-password';
export let reset_password_page_url = serverurl + API_VERSION + 'authentication/reset-password';
export let send_password_reset_link_url = serverurl + API_VERSION + 'authentication/send-password-reset-link';
export let create_profile_url = serverurl + API_VERSION + 'account-management/create-profile';
export let get_profile_picture_url = serverurl + API_VERSION + 'account-management/get-profile-picture';
export let check_completed_profile = serverurl + API_VERSION + 'account-management/check-completed-profile';
export let get_complete_profile = serverurl + API_VERSION + 'account-management/user-profile';
export let upload_document_url = serverurl + API_VERSION + 'account-management/upload-document';


// SIL
export let fetch_users_url = dummyendpoint + 'users';


// generic / unauthenticate
export let get_complete_generic_profile = serverurl + API_VERSION + 'general/generic-user-profile';
export let get_complete_generic_profiles = serverurl + API_VERSION + 'general/generic-user-profiles';



// GENERAL
export let list_staff_url = serverurl + API_VERSION + 'account-management/filter-by-email';
export let get_user_details_url = serverurl + API_VERSION + 'account-management/get-user-details';
export let get_users_url = serverurl + API_VERSION + 'account-management/fetch-users';
export let award_user_role_url = serverurl + API_VERSION + 'superuser/award-role';
export let revoke_user_role_url = serverurl + API_VERSION + 'superuser/revoke-role';
export let user_registration_form_url = serverurl + API_VERSION + 'superuser/user-registration-form';
export let create_user_url = serverurl + API_VERSION + 'superuser/create-user';
export let swap_user_department_url = serverurl + API_VERSION + 'superuser/swap-user-department';
export let suspend_user_url = serverurl + API_VERSION + 'superuser/suspend-user';
export let unsuspend_user_url = serverurl + API_VERSION + 'superuser/un-suspend-user';
export let reset_password_url = serverurl + API_VERSION + 'superuser/reset-user-password';
export let change_password_url = serverurl + API_VERSION + 'account-management/change-password';
export let edit_user_url = serverurl + API_VERSION + 'superuser/edit-user';
export let list_user_roles = serverurl + API_VERSION + 'account-management/list-roles';
export let fetch_roles_url = serverurl + API_VERSION + 'account-management/fetch-roles';
export let refetch_roles_url = serverurl + API_VERSION + 'account-management/re-fetch-roles';
export let list_users_with_role = serverurl + API_VERSION + 'account-management/list-users-with-role';
export let get_user_roles_url = serverurl + API_VERSION + 'account-management/list-user-roles';
export let list_departments = serverurl + API_VERSION + 'department';

