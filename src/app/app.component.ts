import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { IconSetService } from '@coreui/icons-angular';
import { iconSubset } from './icons/icon-subset';
import { Title } from '@angular/platform-browser';
import { AuthenticationService } from './authentication/services/authentication.service';
import { NgxPermissionsService } from 'ngx-permissions';
import { get_user_roles_url } from './app.constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'body',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {
  title = 'Moments';

  constructor(
    private router: Router,
    private titleService: Title,
    private iconSetService: IconSetService
    ,public authservice: AuthenticationService, private permissionsService: NgxPermissionsService
  ) {
    titleService.setTitle(this.title);
    // iconSet singleton
    this.checkifAuthenticated();

    iconSetService.icons = { ...iconSubset };
  }
  disable_console() {
    // console.log = function() {};
   }

  checkifAuthenticated() {
    this.authservice.authenticationState.subscribe(state => {
      if (state) {
        const payload = {

        };

        this.authservice.getrecords(get_user_roles_url, payload).subscribe((res:any) => {
          const all_roles = res['group_name'];
          console.log('assinged roles', all_roles);
          this.permissionsService.addPermission(all_roles, (permissionName:any, permissionsObject) => {
            return !!permissionsObject[permissionName];
        });
        // this.router.navigate(['/landing/home']);
        // this.router.navigate(['/channel/profiles']);
        const pathname:any = localStorage.getItem('ROUTE_TO');
          if (pathname){
            localStorage.removeItem('ROUTE_TO');
            this.router.navigate([pathname]);
          } else{
            // this.router.navigate(['/channel/profiles']);
            this.router.navigate(['/landing/home']);
          }  
        });

      } else {
        let current_url = window.location.href;
        if(current_url.search('generic') !== -1){
          // do nothing`
        } else {
          // this.router.navigate(['authentication/login']);        
          // this.router.navigate(['channel/profile/bobkings']);     
          const pathname:any = localStorage.getItem('ROUTE_TO');
          if (pathname){
            localStorage.removeItem('ROUTE_TO');
            this.router.navigate([pathname]);
          } else{
            this.router.navigate(['/moments/home']);  
          }    
        }
      }

    });
  }
  fetchpermissions() {


  }
  ngOnInit(): void {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
    });
  }
}
