import { TestBed, async, inject } from '@angular/core/testing';

import { VerifyEmailGuard } from './verify-email.guard';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { NgxPermissionsModule } from 'ngx-permissions';

describe('VerifyEmailGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VerifyEmailGuard, JwtHelperService],
      imports: [RouterTestingModule,HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
        JwtModule.forRoot({ // for JwtHelperService
          config: {
            tokenGetter: () => {
              return '';
            }
          }
        }),
        NgxPermissionsModule.forRoot()
      ]
    });
  });

  it('should ...', inject([VerifyEmailGuard], (guard: VerifyEmailGuard) => {
    expect(guard).toBeTruthy();
  }));
});
