import { TestBed, async, inject } from '@angular/core/testing';

import { ChangePasswordGuard } from './change-password.guard';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { NgxPermissionsAllowStubDirective, NgxPermissionsModule } from 'ngx-permissions';

describe('ChangePasswordGuard', () => {
  let guard: ChangePasswordGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChangePasswordGuard, JwtHelperService],
      declarations: [
        NgxPermissionsAllowStubDirective
      ],
      imports: [RouterTestingModule, HttpClientTestingModule,
        JwtModule.forRoot({ // for JwtHelperService
          config: {
            tokenGetter: () => {
              return '';
            }
          }
        }),
        NgxPermissionsModule.forRoot()
      ]
    });
    guard = TestBed.inject(ChangePasswordGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

});
