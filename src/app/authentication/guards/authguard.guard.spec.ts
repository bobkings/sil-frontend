import { TestBed } from '@angular/core/testing';

import { AuthenticationGuard } from './authguard.guard';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { NgxPermissionsAllowStubDirective, NgxPermissionsModule } from 'ngx-permissions';

describe('AuthenticationGuard', () => {
  let guard: AuthenticationGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule,
        JwtModule.forRoot({ // for JwtHelperService
          config: {
            tokenGetter: () => {
              return '';
            }
          }
        }),
        NgxPermissionsModule.forRoot()
      ],
      providers: [
        AuthenticationGuard,
        JwtHelperService
      ],
      declarations: [
        NgxPermissionsAllowStubDirective
      ],
    });
    guard = TestBed.inject(AuthenticationGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
