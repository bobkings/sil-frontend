import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewGenericProfileComponent } from './view.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ToastrModule } from 'ngx-toastr';
import { NgxPermissionsAllowStubDirective, NgxPermissionsModule } from 'ngx-permissions';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { CardModule, ListGroupModule } from '@coreui/angular';

describe('ViewGenericProfileComponent', () => {
  let component: ViewGenericProfileComponent;
  let fixture: ComponentFixture<ViewGenericProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewGenericProfileComponent,NgxPermissionsAllowStubDirective ],
      imports: [RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        ToastrModule.forRoot(),
        JwtModule.forRoot({ // for JwtHelperService
          config: {
            tokenGetter: () => {
              return '';
            }
          }
        }),
        NgxPermissionsModule.forRoot(),
        CardModule,
        ListGroupModule
      ],
      providers: [
        JwtHelperService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewGenericProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
