import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SweetalertService } from '../../../common-module/shared-service/sweetalerts.service';
import { ToastService } from '../../../common-module/shared-service/toast.service';
import { LoadingService } from '../../../common-module/shared-service/loading.service';
import { dummyendpoint, serverurl } from '../../../app.constants';
import { AuthenticationService } from '../../../authentication/services/authentication.service';
import { AdministrationService } from '../../../administration/services/administration.service';
import { ViewChild } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-album-profile',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumGenericProfileComponent implements OnInit {
  picture_link: any= null;
  form_type = 'create';
  is_primary:any = null;
  active = 1;
  posts_num = 0;
  images_num = 0;
  videos_num = 0;
  serverurl = serverurl

  @ViewChild('staticTabs', { static: false}) staticTabs: TabsetComponent
  cover_link: string = '';
  user_data: { email: any; name: any; user_id: any; bio: any; };
  user_id: any;
  user_is_authenticated: boolean = false;
  is_subscribed: any;
  media: any;
  spin: boolean;
  type: any;
  url: any;
  user: any;
  album_id: any;
  album_name: string | null;


  onNavChange(changeEvent: NgbNavChangeEvent) {
    if (changeEvent.nextId === 3) {
      changeEvent.preventDefault();
    }
  }
  
  constructor(private formBuilder: FormBuilder,
     public sweetalertService: SweetalertService, public toastService: ToastService,
      public loadingService: LoadingService,
      public router: Router,
      public administrationService: AdministrationService,
      public authenticationService: AuthenticationService,
      private route: ActivatedRoute,) {
   

    this.user_id = this.route.snapshot.paramMap.get('user_id');
    this.album_id = this.route.snapshot.paramMap.get('album_id');
    this.album_name = this.route.snapshot.paramMap.get('album_name');
    this.get_media(this.album_id);
    this.get_user(this.user_id);
    this.is_authenticated();

  }


  get_media(album_id:number){
    // fetches an album's photos
    const payload = {
    }
    this.loadingService.showloading();
    const url = dummyendpoint + 'albums/' + album_id + '/photos'
    this.administrationService.getrecords(url,payload).subscribe((res:any) => {
      if(res) {
        console.log(res);
        this.media = res;
        this.loadingService.hideloading();
      }
    })
  }

  get_user(user_id:number){
    // fetches album's owner details
    const payload = {
    }
    const url = dummyendpoint + 'users/' + user_id
    this.administrationService.getrecords(url,payload).subscribe((res:any) => {
      if(res) {
        console.log(res);
        this.user = res;
      }
    })
  }

  edit_profile(){
    this.router.navigate(['/profile/create']);
  }



  is_authenticated(){
    // checks user authentication status
    const is_auth = this.authenticationService.authenticationState.value;
    this.user_is_authenticated = is_auth;
  }

  get_authenticated(){
    // reroutes user to authentication page
    this.sweetalertService.showAlert('', 'Login / Signup to continue...', 'info');
    let pathname = window.location.pathname;
    localStorage.setItem('ROUTE_TO',pathname);
    this.router.navigate(['authentication/login']);
  }

  view_media(url:any,type:any){
    // sets media url
    this.type = type;
    this.url = url
  }

  view_photo(photo_id:any){
    // routes to view specific photo page
    this.router.navigate(['/channel/photo',  this.user_id, photo_id, this.album_name]);
  }


  ngOnInit(): void {
  }

}
