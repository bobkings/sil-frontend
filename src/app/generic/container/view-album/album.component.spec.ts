import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumGenericProfileComponent } from './album.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { NgxPermissionsAllowStubDirective, NgxPermissionsModule } from 'ngx-permissions';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CardModule } from '@coreui/angular';

describe('AlbumGenericProfileComponent', () => {
  let component: AlbumGenericProfileComponent;
  let fixture: ComponentFixture<AlbumGenericProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumGenericProfileComponent, NgxPermissionsAllowStubDirective ],
      providers: [
        JwtHelperService
      ],
      imports: [RouterTestingModule, HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        JwtModule.forRoot({ // for JwtHelperService
          config: {
            tokenGetter: () => {
              return '';
            }
          }
        }),
        NgxPermissionsModule.forRoot(),
        ModalModule,
        CardModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumGenericProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
