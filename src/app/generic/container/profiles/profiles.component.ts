import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SweetalertService } from '../../../common-module/shared-service/sweetalerts.service';
import { ToastService } from '../../../common-module/shared-service/toast.service';
import { LoadingService } from '../../../common-module/shared-service/loading.service';
import { serverurl,get_complete_profile,create_profile_url, upload_document_url, get_complete_generic_profiles } from '../../../app.constants';
import { AuthenticationService } from '../../../authentication/services/authentication.service';
import { AdministrationService } from '../../../administration/services/administration.service';
import { ViewChild } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.css']
})
export class GenericProfilesComponent implements OnInit {
  active = 1;
  serverurl = serverurl;

  @ViewChild('staticTabs', { static: false}) staticTabs: TabsetComponent
  cover_link: string = '';
  user_data: { email: any; name: any; username: any; bio: any; };
  username: any;
  profiles: any;

  
  constructor(private formBuilder: FormBuilder,
     public sweetalertService: SweetalertService, public toastService: ToastService,
      public loadingService: LoadingService,
      public router: Router,
      public administrationService: AdministrationService,
      public authenticationService: AuthenticationService,
      private route: ActivatedRoute,) {
  

    // this.action_required_menu =  this.authenticationService.requiresPasswordChange();

    this.get_complete_profiles()


  }

 


  get_complete_profiles(){
    // gets user profile
    const payload = {
    }
    this.administrationService.getrecords(get_complete_generic_profiles,payload).subscribe((res:any) => {
      if(res) {
        // console.log(res);
        this.profiles = res;
      }
    })
  }
  
  visitPage(username:any){
    this.router.navigate(['/channel/profile', username]);
  }

  ngOnInit(): void {
  }

}
