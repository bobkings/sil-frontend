import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NgxPermissionsGuard } from 'ngx-permissions';
import { AuthenticationGuard } from '../../authentication/guards/authguard.guard';
import { GenericProfilesComponent } from '../container/profiles/profiles.component';
import { ViewGenericProfileComponent } from '../container/view-profile/view.component';
import { AlbumGenericProfileComponent } from '../container/view-album/album.component';
import { PhotoGenericProfileComponent } from '../container/view-photo/photo.component';
const routes: Routes = [


  {
    path: 'profile/:user_id',
    component: ViewGenericProfileComponent,
    data: {
      title: 'View Profile',
      permissions: {
        // only: [''],
        redirectTo: '/500'
      }
    },
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'profiles',
    component: GenericProfilesComponent,
    data: {
      title: 'View Profiles',
      permissions: {
        // only: [''],
        redirectTo: '/500'
      }
    },
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'album/:user_id/:album_id/:album_name',
    component: AlbumGenericProfileComponent,
    data: {
      title: 'View Album',
      permissions: {
        // only: [''],
        redirectTo: '/500'
      }
    },
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'photo/:user_id/:photo_id/:album_name',
    component: PhotoGenericProfileComponent,
    data: {
      title: 'View Photo',
      permissions: {
        // only: [''],
        redirectTo: '/500'
      }
    },
    canActivate: [AuthenticationGuard]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenericProfileRoutingModule { }
