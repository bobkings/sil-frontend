import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { GeneralModule } from '../../common-module/common-module/common-module.module';
import { ViewGenericProfileComponent } from '../container/view-profile/view.component';
import { AlbumGenericProfileComponent } from '../container/view-album/album.component';
import { GenericProfilesComponent } from '../container/profiles/profiles.component';
import { PhotoGenericProfileComponent } from '../container/view-photo/photo.component';

import { AccordionModule } from 'ngx-bootstrap/accordion'
import { GenericProfileRoutingModule } from './generic-routing.module';
import { SpinnerModule } from '@coreui/angular';

@NgModule({
  declarations: [
    ViewGenericProfileComponent, GenericProfilesComponent, AlbumGenericProfileComponent, PhotoGenericProfileComponent
  ],

  imports: [
    GeneralModule,
    CommonModule,
    FormsModule,
    BsDatepickerModule,
    ReactiveFormsModule,
    GenericProfileRoutingModule,
    AccordionModule,
    SpinnerModule
    
  ]
})
export class GenericModule { }

