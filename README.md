
## MOMENTS-UI CHARIOT

MOMENTS is built ontop of Angular14, bootstrap5 framework and GitLab CI/CD.
Welcome to Moments, the premier platform for photo sharing! We believe that every moment is worth capturing, and we're here to make it easy for you to share those memories with the world. Our user-friendly interface makes it simple for you to upload and share your photos, and our robust privacy settings give you complete control over who sees your images.


## Live
Application is live at: http://20.102.106.83:5900/
username: admin
Password: login
Backend service at: http://20.102.106.83:5901/


## Installation

### Clone repo

``` bash
# clone the repo
$ git clone  https://gitlab.com/bobkings/sil-frontend.git

# go into app's directory
$ cd sil-frontend

# install app's dependencies
$ npm install
```

## Usage
``` bash
# serve with hot reload at localhost:4200.
$ ng serve or npm run demo

# build for production with minification
$ ng build
```

## Deployment
``` bash
$ touch .env 
# edit the config with the client server or domain for nginx ie.
  NGINX_SERVER_NAME= <IP Address or Domain>



## Creators

**bobkings**